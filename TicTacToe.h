#include <iostream>
#include <conio.h>

using namespace std;

class TicTacToe
{
private:

	char m_board[9]; // An array used to store the spaces of the game board. Once a player has Moved in one of the positions, the space should change to an X or an O.
	int m_numTurns; // This will be used to detect when the board is full (all nine spaces are taken).
	char m_playerTurn; // This is used to track if it's X's turn or O's turn.
	char m_winner; // This is used to check to see the winner of the game. A space should be used while the game is being played.

public:

	// constructor - initialize variables
	TicTacToe()
	{
		m_playerTurn = 'X';
		m_numTurns = 0;
		m_winner = ' ';

		for (int i = 0; i < 9; i++) m_board[i] = ' ';

	}

	// quick and crude board display
	void DisplayBoard()
	{

		for (int i = 0; i < 9; i++)
		{

			cout << " " << m_board[i] << " |";
			if ((i+1) % 3 == 0) { cout << "\n"; }

		}

		cout << "\n";
	}

	// mark the position on the board and change the player, increase turn count
	void Move(int position)
	{

		m_board[position-1] = m_playerTurn;

		if (m_playerTurn == 'X') { m_playerTurn = 'O'; }
		else { m_playerTurn = 'X'; }

		m_numTurns++;

	}

	void DisplayResult()
	{

		// display result, either win or tie, and number of turns
		if (m_winner == ' ') { cout << "Tie game in " << m_numTurns << " turns.\n\n"; } else
		cout << "Player " << m_winner << " wins in " << m_numTurns << " turns.\n\n";

	}

	bool IsOver()

	{
		

		// check the board for a win
		if ((m_board[0] != ' ') && ((m_board[0] == m_board[1]) && (m_board[1] == m_board[2]))) {
			m_winner = m_board[0]; return true;
		}
		if ((m_board[3] != ' ') && ((m_board[3] == m_board[4]) && (m_board[4] == m_board[5]))) {
			m_winner = m_board[0]; return true;
		}
		if ((m_board[6] != ' ') && ((m_board[6] == m_board[7]) && (m_board[7] == m_board[8]))) {
			m_winner = m_board[0]; return true;
		}
		if ((m_board[0] != ' ') && ((m_board[0] == m_board[3]) && (m_board[3] == m_board[6]))) {
			m_winner = m_board[0]; return true;
		}
		if ((m_board[1] != ' ') && ((m_board[1] == m_board[4]) && (m_board[4] == m_board[7]))) {
			m_winner = m_board[0]; return true;
		}
		if ((m_board[2] != ' ') && ((m_board[2] == m_board[5]) && (m_board[5] == m_board[8]))) {
			m_winner = m_board[0]; return true;
		}
		if ((m_board[0] != ' ') && ((m_board[0] == m_board[4]) && (m_board[4] == m_board[8]))) {
			m_winner = m_board[0]; return true;
		}
		if ((m_board[2] != ' ') && ((m_board[2] == m_board[4]) && (m_board[4] == m_board[6]))) {
			m_winner = m_board[0]; return true;
		}

		// if we got to 9 turns with no winner, the game is over
		if (m_numTurns == 9) { m_winner = ' '; return true; }

		// if no winner was found return false
		return false;

	}

	// return the current player
	char GetPlayerTurn()
	{

		return m_playerTurn;

	}

	// determine if a move is valid by checking if position is blank
	bool IsValidMove(int position)
	{

		return m_board[position-1] == ' ';

	}

};